
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;

public class Gui_Program extends JFrame {
	MenuBar mbar;
	ModelingPanel model;
	//ModelUsage usage;

	int width, height;

	public Gui_Program() {
		//set_Init_value();  // ========> set value , path of dic line 47
		getContentPane().setBackground(new Color(149, 149, 149));
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) { }
		initUI();
	}

	void initUI() {
		getContentPane().setLayout(null);
		mbar = new MenuBar();
		this.setJMenuBar(mbar);

		model = new ModelingPanel();
		model.setBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(105, 105, 105), new Color(230, 230, 250)));
		model.setVisible(true);
		getContentPane().add(model);

		/*usage = new ModelUsage();
		usage.setBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(105, 105, 105), new Color(230, 230, 250)));
		usage.setVisible(false);
		getContentPane().add(usage);*/
	}

	/*private void set_Init_value() {

		Utility_Share.stopWordHSEN = Fileprocess.FileHashSet("stopwordAndSpc_eng.txt");
		Utility_Share.dicHSEN = Fileprocess.FileHashSet("dictionaryEn.txt");
		
		// class for test and train
		Utility_Share.classes = 5;  
		
		Utility_Share.algorithm = "naive";
		Utility_Share.typeweighting = "tf-idf";
		
		// parameter for KNN
		Utility_Share.K = 7;   //3, 5, 7, 11
		
		// parameter for BM25
		Utility_Share.k = 0.5;
		Utility_Share.k1 = 1;
		Utility_Share.b = 0.5;
	}*/

	/*
	 * InnerClass <MenuBar>
	 */
	class MenuBar extends JMenuBar implements ActionListener {
		JButton mnModeling, mnModelUsage, mnAbout, mnExit, mnMinimize;
		public MenuBar() {
			mnModeling = new JButton("Modeling");
			mnModeling.setFocusable(false);
			mnModeling.addActionListener(this);
			this.add(mnModeling);

			mnModelUsage = new JButton("Model Usage");
			mnModelUsage.setFocusable(false);
			mnModelUsage.addActionListener(this);
			this.add(mnModelUsage);
			
			mnAbout = new JButton("About us");
			mnAbout.setFocusable(false);
			mnAbout.addActionListener(this);
			// this.add(mnAbout);

			mnMinimize = new JButton("Minimize");
			mnMinimize.setFocusable(false);
			mnMinimize.setBackground(Color.BLUE);
			mnMinimize.setForeground(Color.WHITE);
			mnMinimize.addActionListener(this);
			this.add(mnMinimize);

			mnExit = new JButton("Exit");
			mnExit.setFocusable(false);
			mnExit.setBackground(Color.RED);
			mnExit.setForeground(Color.WHITE);
			mnExit.addActionListener(this);
			this.add(mnExit);
		}

		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(mnExit))
				System.exit(0); // close Program
			else if (e.getSource().equals(mnMinimize))
				setState(ICONIFIED);
			/*else if (e.getSource().equals(mnAbout))
				new About();
			else if (e.getSource().equals(mnModeling)) {
				model.setVisible(true); //=================> ModelingPanel.java
				usage.setVisible(false);//=================> ModelUsage.java
			} else if (e.getSource().equals(mnModelUsage)) {
				model.setVisible(false);//=================> ModelingPanel.java
				usage.setVisible(true);//=================> ModelUsage.java
			}*/
		}

	}
}