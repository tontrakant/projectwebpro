import java.awt.Toolkit;


public class MyMain {

	public static void main(String[] args) {
		Gui_Program pro = new Gui_Program();
		//pro.setIconImage(Toolkit.getDefaultToolkit().getImage(pro.getClass().getResource("icon.png")));
		pro.setExtendedState(pro.MAXIMIZED_BOTH);
		pro.setLocationRelativeTo(null);
		pro.setUndecorated(true);
		pro.setDefaultCloseOperation(pro.EXIT_ON_CLOSE);
		pro.setVisible(true);
	}
}
