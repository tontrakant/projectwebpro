import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class ModelingPanel extends JPanel implements Runnable, ActionListener {
	ArrayList<String[]> data = new ArrayList<String[]>();
	JTextField path;
	JRadioButton rdbtnNaive, rdbtnKNN;
	JButton btnLoad, btnStartTraining, btnStopTraining, btnClear, btnClearTS;
	static JButton btnStartTesting;
	JTextField pathFile;
	JProgressBar progressBar, progressBar_1;

	JRadioButton rdb_tf, rdb_tf_idf, rdb_BM25;
	public static JTextArea textTrain, textTest;

	ButtonGroup buttonGroup_algorithm = new ButtonGroup();
	ButtonGroup buttonGroup_weighting = new ButtonGroup();

	Image bg;
	String /* Fullstr = "Sentiment Analyzer Modeling ", */ tmpStr = "";

	Thread runThread;

	public ModelingPanel() {
		// TODO Auto-generated constructor stub
		initPanel();
	}

	void initPanel() {
		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		float width = screenSize.width;
		float height = screenSize.height;
		int xLocation = (int) ((width - 1024) / 2);
		int yLocation = (int) ((height - 768) / 2);

		setLocation(xLocation, yLocation);
		setSize(1024, 736);
		setLayout(null);

/////////////////////////////////////////////////////////////////////////////////////////////////

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(252, 240, 520, 250);
		add(scrollPane);

		textTrain = new JTextArea();
		textTrain.setFocusable(false);
		textTrain.setEditable(false);
		scrollPane.setViewportView(textTrain);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(252, 525, 520, 175);
		add(scrollPane_1);

		textTest = new JTextArea();
		textTest.setFocusable(false);
		textTest.setEditable(false);
		scrollPane_1.setViewportView(textTest);

		progressBar = new JProgressBar();
		progressBar.setBounds(832, 380, 146, 25);
		// progressBar.setBounds(35, 550, 141, 23);
		add(progressBar);

		/////////////////// Training /////////////////////////
		btnLoad = new JButton("Load Files");
		btnLoad.setBounds(695, 164, 110, 36);
		btnLoad.setBackground(Color.BLACK);
		btnLoad.setForeground(Color.WHITE);
		btnLoad.setFont(new Font("Arial", Font.PLAIN, 16));
		btnLoad.addActionListener(this);
		add(btnLoad);

		btnStartTraining = new JButton("Start Training");
		btnStartTraining.setBounds(832, 227, 148, 33);
		btnStartTraining.setBackground(Color.BLACK);
		btnStartTraining.setForeground(Color.WHITE);
		btnStartTraining.setFont(new Font("Arial", Font.PLAIN, 16));
		// btnStartTraining.setForeground(Color.lightGray);
		btnStartTraining.addActionListener(this);
		add(btnStartTraining);

		btnStopTraining = new JButton("Stop Training");
		btnStopTraining.setEnabled(false);
		btnStopTraining.setBounds(832, 263, 148, 34);
		btnStopTraining.setBackground(Color.BLACK);
		btnStopTraining.setForeground(Color.WHITE);
		btnStopTraining.setFont(new Font("Arial", Font.PLAIN, 16));
		// btnStopTraining.setBounds(35, 470, 141, 23);
		btnStopTraining.addActionListener(this);
		add(btnStopTraining);

		btnClear = new JButton("Clear");
		btnClear.setBounds(832, 301, 148, 33);
		btnClear.setBackground(Color.BLACK);
		btnClear.setForeground(Color.WHITE);
		btnClear.setFont(new Font("Arial", Font.PLAIN, 16));
		// btnClear.setBounds(35, 500, 141, 23);
		btnClear.addActionListener(this);
		add(btnClear);

		///////////////////// Testing //////////////////////////
		/*
		 * JLabel lblSizeOfTest = new JLabel("Size of Test Files");
		 * lblSizeOfTest.setHorizontalAlignment(SwingConstants.CENTER);
		 * lblSizeOfTest.setForeground(Color.WHITE); lblSizeOfTest.setBounds(850, 515,
		 * 125, 14); add(lblSizeOfTest);
		 */

		pathFile = new JTextField();
		pathFile.setHorizontalAlignment(SwingConstants.RIGHT);
		pathFile.setBounds(835, 515, 140, 31);
		add(pathFile);
		pathFile.setColumns(10);

		btnStartTesting = new JButton("Start Testing");
		btnStartTesting.setBounds(831, 557, 148, 33);
		btnStartTesting.setBackground(Color.BLACK);
		btnStartTesting.setForeground(Color.WHITE);
		btnStartTesting.setFont(new Font("Arial", Font.PLAIN, 16));
		btnStartTesting.addActionListener(this);
		add(btnStartTesting);

		btnClearTS = new JButton("Clear");
		btnClearTS.setBounds(831, 593, 148, 33);
		btnClearTS.setBackground(Color.BLACK);
		btnClearTS.setForeground(Color.WHITE);
		btnClearTS.setFont(new Font("Arial", Font.PLAIN, 16));
		btnClearTS.addActionListener(this);
		add(btnClearTS);

		progressBar_1 = new JProgressBar();
		progressBar_1.setBounds(832, 661, 146, 25);
		add(progressBar_1);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		ArrayList<String> data = new ArrayList<String>();
		if (e.getSource().equals(btnLoad)) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader("data.csv"));
				String s = reader.readLine();
				while (s != null) {
					data.add(s);
					s = reader.readLine();
				}
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for (String s : data) {
				this.data.add(s.split(","));
				for (String ss : s.split(","))
					textTrain.append(ss + "\t");
				textTrain.append("\n");
			}

		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}
