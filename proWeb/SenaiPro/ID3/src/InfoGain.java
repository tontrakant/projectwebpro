import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InfoGain {

	private int num_class = 0;
	private int num_attr = 0;
	private int data[][];
	
	private float entropy 	= -1.0f;
	private float gain 		= -1.0f;
	
	private String gain_name		= "";
	String attr_label[][];
	String label[];
	String child_size[];
	
	int count_distinct[];
	int distinct_table[][];
	
	float infoCol = -1;
	
	public InfoGain(int data_in[][],Load_data raw_data) {
		data = data_in;
		num_class = raw_data.getNum_class();
		num_attr = raw_data.getNum_attr();
		attr_label = raw_data.getAttr_label();
		label = transpose(attr_label)[0];
		child_size = transpose(attr_label)[1];
	}
	
	public void doInfoDataSet(int idx){
		infoCol = infoColumn(mention(data,idx)) * (-1);
		System.out.println("info of dataset : "+infoCol);
	}
	
	
	public void doInfoGain(int idx){
		
			System.out.println("Attribute : "+label[idx]);
			
			String _label[] = transpose(attr_label)[2][idx].split(",");
			String _label_sub[] = transpose(attr_label)[3][idx].split(",");
			float sum = 0.0f;
			distinct_table = new int[_label.length][];
			
		for(int i=0;i<mention(data,idx).length;i++){
				/*for (int j = 0; j < mention(data,idx)[i].length; j++) {
					System.out.print(mention(data,idx)[i][j]+" ");
				}
				System.out.println();*/
			}
			for (int j = 0; j < _label.length; j++) {
				System.out.println("\nrun on branch : "+_label_sub[j]);
				float t_info = infoAttr(mention(data,idx),_label[j]);
				sum += t_info;
			//	if(t_info !=0)
					System.out.println("info("+transpose(attr_label)[0][idx]+","+_label_sub[j]+") --> "+t_info);
				distinct_table[j] = count_distinct;
			}
			
			entropy = (sum*=-1);
			System.out.println("\n--- Entropy = "+entropy);
			gain = (infoCol-(sum));
			System.out.println("--- Gain("+label[idx]+") = "+gain+"\n");
			System.out.println("--------------------------------\n");
	}
	
	public int[][] mention(int data[][],int focus_column){
		int answer[][] = new int[2][];
		answer[0] = transpose(data)[focus_column];
		answer[1] = transpose(data)[data[0].length-1];
		return transpose(answer);
	}
	
	public float infoColumn(int input[][]){
		float answer = 0.0f;
		int tr[][] = transpose(input);
		int loop[] = distinct(tr[1]);
		int num_row = input.length;
		
		for (int i = 0; i < loop.length; i++) {
			loop[i] = count(tr[1],loop[i]);
			float pr_i;
			if(loop[i] == 0){
				pr_i = 0.0f;
			}else{
				pr_i = (float)loop[i]/(float)num_row;
			}
			float t_info = pr_i*log(loop[i],num_row,2);
			answer += t_info;
			System.out.println("(-1) x [("+loop[i]+"/"+num_row+") x ("+log(loop[i],num_row,2)+")] = "+(t_info*-1));
		}
		int y[] = new int[num_class];
		for (int i = 0; i < tr[1].length; i++) {
			if(tr[1][i]==1){
				y[0]++;
			}else{
				y[1]++;
			}
		}
		count_distinct = y;
		System.out.println(Arrays.toString(loop)+" = "+(answer*-1));
		return answer;
	}

	public float infoAttr(int input[][], String condition) {
		float answer = 0.0f;
		int num_row = input.length;
		input = reData(input, 0, condition);
		
		if(input.length != 0){
			int tr[][] = transpose(input);
			int loop[] = distinct(tr[1]);
			for (int i = 0; i < loop.length; i++) {
				loop[i] = count(tr[1], loop[i]);
			}
			float inC = infoColumn(input);
			answer = ((((float) (loop[0] + loop[1]) / (float) num_row)) * inC);
		}
		
		return answer;
	}
	private int[] distinct(int input[]){
		ArrayList<Integer> answer = new ArrayList<Integer>();
		for (int i = 0; i < input.length; i++) {
			if(!answer.contains(input[i])){
				answer.add(input[i]);
			}
		}
		int x[] = new int[num_class];
		for (int i = 0; i < x.length; i++) {
			if(i>=answer.size()){
				x[i] = 0;
			}else{
				x[i] = answer.get(i);
			}
		}
		Arrays.sort(x);
		return x;
	}
	private int count(int input[],int condition){
		int answer = 0;
		for (int i = 0; i < input.length; i++) {
			if(condition == input[i]){
				answer++;
			}
		}
		return answer;
	}
	public float log(int a,int b, int base) {
		if(a == 0){return 0;} // trows exception
		
	    return (float) (Math.log((float)a/(float)b) / Math.log(base));
	}
	
	public String[][] transpose(String [][] m){
		String[][] temp = new String[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[i].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	public int[][] transpose(int [][] m){
		int[][] temp = new int[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[i].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	public int[][] reData(int input[][],int column,String condition){
		List<int[]> list = new ArrayList<int[]>(Arrays.asList(input));//Arrays.asList(input);
		int ix = 0;
		for (int i = 0; i < input.length; i++) {
			if(input[i][column] != Integer.parseInt(condition)){
				list.remove(i-ix);
				ix++;
			}
		}
		return list.toArray(new int[][]{});
	}

	public int getNum_class() {
		return num_class;
	}

	public void setNum_class(int num_class) {
		this.num_class = num_class;
	}

	public int getNum_attr() {
		return num_attr;
	}

	public void setNum_attr(int num_attr) {
		this.num_attr = num_attr;
	}

	public int[][] getData() {
		return data;
	}

	public void setData(int[][] data) {
		this.data = data;
	}

	public float getEntropy() {
		return entropy;
	}

	public void setEntropy(float entropy) {
		this.entropy = entropy;
	}

	public float getGain() {
		return gain;
	}

	public void setGain(float gain) {
		this.gain = gain;
	}

	public String getGain_name() {
		return gain_name;
	}

	public void setGain_name(String gain_name) {
		this.gain_name = gain_name;
	}

	public String[][] getAttr_label() {
		return attr_label;
	}

	public void setAttr_label(String[][] attr_label) {
		this.attr_label = attr_label;
	}

	public String[] getLabel() {
		return label;
	}

	public void setLabel(String[] label) {
		this.label = label;
	}
}
