import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ID3_class {
	
	private Load_data raw_data;
	private int data[][];
	
	private String label_name[];
	private int pointer = 0;
	private int maxIdx = -1;
	private List<Integer> seleted = new ArrayList<Integer>();
	
	Node_tree result_tree; // for root node
	
	ID3_class(Load_data loaded){
		this.raw_data = loaded;
	}

	ID3_class(String file_name){
		this.raw_data = new Load_data(file_name);
		this.data = raw_data.getData();
	}
	
	public InfoGain[] doGain(){
		String label[] = transpose(raw_data.getAttr_label())[0];
		InfoGain answer[] = new InfoGain[this.raw_data.getNum_attr()];
		
		for(int i=0;i<raw_data.getNum_attr();i++){
			if(seleted.contains(i))
				continue; // skip selected node
			System.out.println(label[i]);
			answer[i] = new InfoGain(data,raw_data);
			System.out.println("\n====== info of dataset =====");
			answer[i].doInfoDataSet(i);
			System.out.println("\n====== info of attributes =====");
			answer[i].doInfoGain(i);
			answer[i].setGain_name(label[i]);
		}
		return answer;
	}
	
	public InfoGain maxGain(InfoGain[] input){
		float max_val = -1;
		int idx = -1;
		for (int i = pointer; i < input.length; i++) {
			if(seleted.contains(i))
				continue;
			float temp = input[i].getGain();
			if(temp > max_val){
				max_val = temp;
				idx = i;
			}
		}
		maxIdx = idx;
		seleted.add(idx);
		return input[idx];
	}
	
	public void expand(Node_tree ex_from,ID3_class input,int max_idx){
		Load_data raw = input.getRaw_data();
		String attr_condition[] = input.getRaw_data().getAttr_label()[max_idx][3].split(",");
		String condition[] = input.getRaw_data().getAttr_label()[max_idx][2].split(",");
		System.out.println("expand from : "+ex_from.getNode_name()+" on "+Arrays.toString(attr_condition));
		
		for (int i = 0; i < attr_condition.length; i++) {
			int temp_dis[] = ex_from.getDistinct_table()[i];
			System.out.print(ex_from.getNode_name()+"|"+Arrays.toString(temp_dis));
			ex_from.children[i] = new Node_tree();
			if(temp_dis[0] == 0 || temp_dis[1] == 0){ // static 2 class
				ex_from.children[i].setIm_leaf(true);
				String res = (temp_dis[0]==0?"2":"1");
				ex_from.children[i].setLeaf_val("Accepted: "+res+" / "+Arrays.toString(temp_dis));
				ex_from.children[i].setBr_name(attr_condition[i]);
				ex_from.children[i].setNode_name("Stoped Node");
				System.out.println("   <-- stop here");
			}else{
				System.out.println("   <-- expand");
				int f_data[][] = input.reData(ex_from.getData(),ex_from.getIdx_max_val(),condition[i]);
				disp_data(f_data);
				ID3_class n_id3 = new ID3_class(raw);
				n_id3.setSeleted(input.getSeleted());
				n_id3.setData(f_data);
				InfoGain temp[] = n_id3.doGain();
				n_id3.maxGain(temp);
				int maxidx = n_id3.getMaxIdx();
				System.out.println(maxidx);
				ex_from.children[i] = new Node_tree(temp[n_id3.getMaxIdx()].getGain_name());
				ex_from.children[i].setBr_name(attr_condition[i]);
				ex_from.children[i].setData(temp[n_id3.getMaxIdx()].getData());
				ex_from.children[i].setDistinct_table(temp[n_id3.getMaxIdx()].distinct_table);
				ex_from.children[i].setIdx_max_val(n_id3.getMaxIdx());
				ex_from.children[i].setDistinct(temp[n_id3.getMaxIdx()].count_distinct);
				ex_from.children[i].setChildren(temp[n_id3.getMaxIdx()].child_size[n_id3.getMaxIdx()]);
				
				n_id3.expand(ex_from.children[i],n_id3,n_id3.getMaxIdx()); // do recursive
				
			}
			
		}
	}
	
	/*
	 * General Function 
	 * */
	
	public void disp_data(String label_n[],int data_input[][]){
		for(int i=0;i<label_n.length;i++){
			System.out.print(label_n[i]+"\t");
		}
		System.out.println("\n------------------------------------------");
		for(int i=0;i<data_input.length;i++){
			for(int j=0;j<data_input[i].length;j++){
				System.out.print(data_input[i][j]+"\t");
			}
			System.out.println();
		}
	}
	public void disp_data(int data_input[][]){
		System.out.println("\n------------------------------------------");
		for(int i=0;i<data_input.length;i++){
			for(int j=0;j<data_input[i].length;j++){
				System.out.print(data_input[i][j]+"\t");
			}
			System.out.println();
		}
	}
	
	public String[][] transpose(String [][] m){
		String[][] temp = new String[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[i].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	
	public boolean isLoadData(){
		if(data == null){
			return false;
		}else{
			return true;
		}
	}

	public int[][] reData(int input[][],int column,String condition){
		List<int[]> list = new ArrayList<int[]>(Arrays.asList(input));//Arrays.asList(input);
		int ix = 0;
		for (int i = 0; i < input.length; i++) {
			if(input[i][column] != Integer.parseInt(condition)){
				list.remove(i-ix);
				ix++;
			}
		}
		return list.toArray(new int[][]{});
	}

	
	/*
	 * Getter and Setter Function
	 * */

	public List<Integer> getSeleted() {
		return seleted;
	}

	public void setSeleted(List<Integer> seleted) {
		this.seleted = seleted;
	}

	public int getPointer() {
		return pointer;
	}

	public void setPointer(int pointer) {
		this.pointer = pointer;
	}
	
	public Load_data getRaw_data() {
		return raw_data;
	}
	
	public int[][] getData() {
		return data;
	}
	

	public void setData(int[][] data) {
		this.data = data;
	}

	public int getMaxIdx() {
		return maxIdx;
	}

	public void setMaxIdx(int maxIdx) {
		this.maxIdx = maxIdx;
	}
	
	
}
