import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Load_data {
	
	/*
	 * Author : Natthakit Srikanjanapert
	 * Sample Code for CS MSU bachalor's degree
	 * */
	
	private int 	num_attr 	= 0;
	private String 	attr_label[][];
	private String 	attr[];
	String 	attr_A[];
	String str = "";
	private int 	num_class 	= 0;
	private String 	class_label[];
	
	private int 	num_records = 0;
	private int 	data[][];
	
	public Load_data() {
	}
	private void load_raw_data(String input_file){
		try {
			BufferedReader buff = new BufferedReader(new FileReader(input_file));
			num_attr = Integer.parseInt(buff.readLine());
			attr_label = new String[num_attr][];
			attr_A = new String[num_attr];
			for(int i=0;i<attr_label.length;i++){
				attr_label[i] = buff.readLine().split(" ");
				attr = Arrays.toString(attr_label[i]).split(" ");
				System.out.println(Arrays.toString(attr_label[i]));
				System.out.println(attr[3]);
				str = "";
				for (int j = 0; j < attr[3].length()-1; j++) {
					str += attr[3].charAt(j);
				}
				attr_A[i] = str;
			}
			num_class = Integer.parseInt(buff.readLine());
			class_label = buff.readLine().split(",");
			num_records = Integer.parseInt(buff.readLine());
			data = new int[num_records][];
			String line = buff.readLine();
			int init_idx = 0;
			while(line!=null){
				String temp_[] = line.split("\t");
				data[init_idx] = new int[temp_.length];
				for (int i = 0; i < temp_.length; i++) {
					data[init_idx][i] = Integer.parseInt(temp_[i]);
				}
				init_idx++;
				line = buff.readLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("error : file not founds ..");
		} catch (IOException e) {
			System.out.println("error : read in line ..");
		}
	}
	
	public int getNum_attr() {
		return num_attr;
	}

	public String[][] getAttr_label() {
		return attr_label;
	}

	public int getNum_class() {
		return num_class;
	}

	public int getNum_records() {
		return num_records;
	}

	public String[] getClass_label() {
		return class_label;
	}
	public String[] attr() {
		return attr;
	}
	public int[][] getData() {
		return data;
	}
	
	public Load_data(String f_name) {
		load_raw_data(f_name);
	}

}
