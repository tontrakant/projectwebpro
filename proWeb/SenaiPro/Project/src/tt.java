import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
  
/**
 * <p>Title: JTextArea as JTable</p>
 * <p>Description: Using JTextArea instead of JTable, but not robust... :wink: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: <Not attributable></p>
 * @author Ko Ko Naing
 * @version 1.0
 */
  
public class tt extends JFrame {
  JPanel contentPane;
  BorderLayout borderLayout1 = new BorderLayout();
  JTextArea jTextArea = new JTextArea();
  
  public static void main(String args[]){
    tt jtat = new tt();
    jtat.setSize(400, 200);
    jtat.setVisible(true);
  }
  //Construct the frame
  public tt() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  
  //Component initialization
  private void jbInit() throws Exception  {
    contentPane = (JPanel) this.getContentPane();
    jTextArea.setText("Items\tPrice\tQTY\tTotals\n");
    jTextArea.append("Pants\t5.89\t2\t?\n");
    jTextArea.append("Shoes\t2.33\t4\t?\n");
    jTextArea.append("Sandals\t3.00\t5\t?\n");
     
    contentPane.setLayout(borderLayout1);
    this.setSize(new Dimension(400, 300));
    this.setTitle("JTextArea as JTable");
    contentPane.add(jTextArea, BorderLayout.CENTER);
  }
  
  //Overridden so we can exit when window is closed
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      System.exit(0);
    }
  }
}