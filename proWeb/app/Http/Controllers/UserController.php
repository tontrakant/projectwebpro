<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{


	public function home(){

		return view('home');
	}
	public function about(){
		return view('about');
	}
	public function roomsrates(){
		$rooms = DB::table('room')->get();
		return view('rooms-rates',['rooms'=>$rooms]);
	}
	public function location(){
		return view('location');
	}
	public function promotion(){
		$promotions = DB::table('promotion')->get();
		return view('promotion',['promotions'=>$promotions]);
	}
	public function book(){
		$rooms = DB::table('room')->get();
		return view('book',['rooms'=>$rooms]);
	}
	public function register(){
		return view('register');
	}
	public function login(Request $request){
		session_start();
		$data = $request->all();
		$d = array(
			'username'=>$data['username'],
			'password'=>$data['password']
		);
		$user = DB::table('users')->where($d)->get();
		if(count($user) == 1){
			foreach ($user as $us) {
				$_SESSION['name'] = $us->name;
				$_SESSION['id'] = $us->id;
				$_SESSION['status'] = $us->status;
			}
			return redirect('home');
		}
		$user = DB::table('admin')->where($d)->get();
		if (count($user) == 1) {
			foreach ($user as $us) {
				$_SESSION['name'] = $us->name;
				$_SESSION['id'] = $us->id;
				$_SESSION['status'] = $us->status;
			}
			return redirect('admin');
		}
		$user = DB::table('boss')->where($d)->get();
		if (count($user) == 1) {
			foreach ($user as $us) {
				$_SESSION['name'] = $us->username;
				$_SESSION['id'] = $us->id;
				$_SESSION['status'] = $us->status;
			}
			return redirect('boss');
		}
		$user = DB::table('employee')->where($d)->get();
		if (count($user) == 1) {
			foreach ($user as $us) {
				$_SESSION['name'] = $us->name;
				$_SESSION['id'] = $us->id;
				$_SESSION['status'] = $us->status;
			}
			return redirect('employee');
		}
		return redirect('home');

	}
	public function logout(){
		session_start();
		session_destroy();
		return redirect('home');
	}
	public function insert_user(Request $request){
		$data = $request->all();
		$d = array(
			'username'=>$data['user'],
			'password'=>$data['pass'],
			'name'=>$data['name'],
			'phone'=>$data['phone'],
			'email'=>$data['email'],
			'info_status'=>'0',
			'status'=>'user'
		);
		DB::table('users')->insert($d);
		return redirect('home');
	}
	public function admin(){
		session_start();
		if(isset($_SESSION['status'])){
			if($_SESSION['status']=='admin'){
				return view('/backend/admin');
			}
		}
		return redirect('home');
	}
	public function status(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='user'){
			$book = DB::table('booking')->get();
			$room = DB::table('booking')
			->join('room', 'booking.id_room', '=', 'room.id')
			->select('name')->where('booking.id_customer','=',$_SESSION['id'])
			->get();
			$data = array(
				'book' => $book,
				'room' => $room
			);
			return view('status' , ['data' => $data] );
		}
		return redirect('home');
	}
	public function  detailstatus($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='user'){
			$data = DB::table('booking')->where('id',$id)->get(); 
			$name = DB::table('users')->where('id',$data[0]->id_customer)->get();
			$room = DB::table('booking')
			->join('room', 'booking.id_room', '=', 'room.id')
			->select('type','price','name')
			->get();
			$infoemation = array(
				'start' => $data[0]->date_start,
				'stop' => $data[0]->date_end,
				'amount_child' => $data[0]->amount_child,
				'amount_adult' => $data[0]->amount_adult,
				'amount_room' => $data[0]->amount_room,
				'name_room' => $room[0]->name,
				'name' => $name[0]->name,
				'type' => $room[0]->type,
				'price' => $room[0]->price
			);
			return view('detail_status' , ['infoemation' => $infoemation] );
		}
		return redirect('home');
	}
	public function question(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='user'){
			$ques = DB::table('question')->where('question.id_customer','=',$_SESSION['id'])->get();
			$data = array(
				'ques' => $ques
			);
			return view('question' , ['data' => $data]);
		}
		return redirect('home');
	}
	public function addquestion(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='user'){
			return  view('addquestion');
		}
		return redirect('home');
	}
	public function addquestion1(Request $request){
		session_start();
		$data = $request->all();
		$result = array(
			'question' => $data['text'],
			'answer'=>'-',
			'id_customer'=>$_SESSION['id']
		);
		DB::table('question')->insert($result);
		return  redirect('question');
	}
	public function addroom(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			return view('/backend/addroom');
		}
		return redirect('home');
	}
	public function adddataroom(Request $request){
		$err=$this->validate($request, [
			'select_file'  => 'required|image|mimes:jpg,png,gif,jpeg|max:2048'
		]);
		$image = $request->file('select_file');
		$new_name = rand() . '.' . $image->getClientOriginalExtension();
		$image->move(public_path('img'), $new_name);
		$data = $request->all();
		$d = array(
			'name'=>$data['nameroom'],
			'type'=>$data['type'],
			'image' => "img/".$new_name,
			'price'=>$data['price'],
			'bed'=>$data['bed'],
			'people'=>$data['numpeo'],
			'size'=>$data['size'],
		);
		DB::table('room')->insert($d);
		return redirect('admin');
	}
	public function editroom($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			$rooms = DB::table('room')->where('id',$id)->get();
			return view('/backend/editroom',['rooms'=>$rooms]);
		}
		return redirect('home');
	}
	public function updateroom(Request $request){
		session_start();
		$data = $request->all();
		$f = DB::table('room')->select('image')->where(['id'=>$data['id']])->get();
		$file_name = $f[0]->image;
		if ($request->file('select_file')!=NULL){
			$err=$this->validate($request, [
				'select_file'  => 'required|image|mimes:jpg,png,gif,jpeg|max:2048'
			]);
			$image = $request->file('select_file');
			$new_name = rand() . '.' . $image->getClientOriginalExtension();
			$image->move(public_path('img'), $new_name);
			
		}
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			
			$d = array(
				'name'=>$data['nameroom'],
				'type'=>$data['type'],
				'image' =>$file_name,
				'price'=>$data['price'],
				'bed'=>$data['bed'],
				'people'=>$data['numpeo'],
				'size'=>$data['size'],
			);
			DB::table('room')->where('id',$data['id'])->update($d);
			return view('/backend/admin');
		}
		return redirect('home');
	}
	public function deleteroom($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			DB::table('room')->where('id',$id)->delete();
			return redirect('admin');
		}
		return redirect('home');
	}
	public function addemployee(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			return view('/backend/addemployee');
		}
		return redirect('home');
	}
	public function adddataemployee(Request $request){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			$data = $request->all();
			// session_start();
			$d = array(
				'name'=>$data['name'],
				'username'=>$data['username'],
				'password'=>$data['password'],
				'address'=>$data['address'],
				'phone'=>$data['phone'],
				'email'=>$data['email'],
				'id_admin'=>$_SESSION['id'],
				'status'=>'employee'
			);
			DB::table('employee')->insert($d);
			return view('/backend/admin');
		}
		return redirect('home');
	}
	public function editeemployee($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			$employees = DB::table('employee')->where('id',$id)->get();
			return view('/backend/editemployee',['employees'=>$employees]);
		}
		return redirect('home');
	}
	public function updateemployee(Request $request){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			// session_start();
			$data = $request->all();
			$d = array(
				'name'=>$data['name'],
				'username'=>$data['username'],
				'password'=>$data['password'],
				'address'=>$data['address'],
				'phone'=>$data['phone'],
				'email'=>$data['email'],
				'id_admin'=>$_SESSION['id'],
				'status'=>'employee'
			);
			DB::table('employee')->where('id',$data['id'])->update($d);
			return view('/backend/admin');
		}
		return redirect('home');
	}
	public function deleteemployee($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			DB::table('employee')->where('id',$id)->delete();
			return redirect('admin');
		}
		return redirect('home');
	}
	public function  addpromotion(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			return view('/backend/addpromotion');
		}
		return redirect('home');
	}
	public function adddatapromotion(Request $request){
		session_start();
		$err=$this->validate($request, [
			'select_file'  => 'required|image|mimes:jpg,png,gif,jpeg|max:2048'
		]);
		$image = $request->file('select_file');
		$new_name = rand() . '.' . $image->getClientOriginalExtension();
		$image->move(public_path('img'), $new_name);

		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			$data = $request->all();
			$d = array(
				'name'=>$data['name'],
				'description'=>$data['discription'],
				'date_start'=>date('Y/m/d'),
				'date_stop'=>date('Y-m-d',strtotime($data['enddate'])),
				'id_admin'=>$_SESSION['id'],
				'image' => "img/".$new_name
			);
			DB::table('promotion')->insert($d);
			return redirect('admin');
		}
		return redirect('home');
	}
	public function editepromotion($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			$promotions = DB::table('promotion')->where('id',$id)->get();
			return view('/backend/editpromotion',['promotions'=>$promotions]);
		}
		return redirect('home');
	}
	public function updatepromotion(Request $request){
		session_start();
		$data = $request->all();
		$f = DB::table('promotion')->select('image')->where(['id'=>$data['id']])->get();
		$file_name = $f[0]->image;
		if ($request->file('select_file')!=NULL){
			$err=$this->validate($request, [
				'select_file'  => 'required|image|mimes:jpg,png,gif,jpeg|max:2048'
			]);
			$image = $request->file('select_file');
			$new_name = rand() . '.' . $image->getClientOriginalExtension();
			$image->move(public_path('img'), $new_name);
			$file_name="img/".$new_name;
		}
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			
			$d = array(
				'name'=>$data['name'],
				'description'=>$data['discription'],
				'date_stop'=>date('Y-m-d',strtotime($data['enddate'])),
				'id_admin'=>$_SESSION['id'],
				'image' =>$file_name
			);
			DB::table('promotion')->where('id',$data['id'])->update($d);
			return redirect('admin');
		}
		return redirect('home');
	}
	public function deletepromotion($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='admin'){
			DB::table('promotion')->where('id',$id)->delete();
			return redirect('admin');
		}
		return redirect('home');
	}
	public function booking(Request $request){
		session_start();
		if(isset($_SESSION['status'])){
			$data = $request->all();
			$d = array(
				'id_customer'=>$_SESSION['id'],
				'date_start'=>date('Y-m-d',strtotime($data['startdate'])),
				'date_end'=>date('Y-m-d',strtotime($data['enddate'])),
				'date_books'=>date('Y/m/d'),
				'status'=>'NO',
				'id_room'=>$data['bookNowRoom1'],
				'amount_child'=>$data['bookNowKids'],
				'amount_adult'=>$data['bookNowAdults'],
				'amount_room'=>$data['numroom'],
			);
			DB::table('booking')->insert($d);
			return redirect('book');
		}
		return redirect('home');
	}
	//**************************************************
	public function boss(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='boss'){
			$promotion = DB::table('promotion')->get();
			$room = DB::table('room')->get();
			$book = DB::table('booking')->get();
			$users = DB::table('booking')
			->join('users', 'booking.id_customer', '=', 'users.id')
			->select('name')
			->get();
			$data = array(
				'promotion' => $promotion,
				'room' => $room,
				'book' => $book,
				'name' => $users
			);
			return view('backend/boss' , ['data' => $data] );
		}
		return redirect('home');
	}
	public function de1boss($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='boss'){
			$data = DB::table('booking')->where('id',$id)->get(); 
			$name = DB::table('users')->where('id',$data[0]->id_customer)->get();
			$name_room = DB::table('room')->where('id',$data[0]->id_room)->get();
			$information = array(
				'name' => $name[0]->name,
				'name_room' =>$name_room[0]->name,
				'type_room' =>$name_room[0]->type,
				'price' =>$name_room[0]->price,
				'child' =>$data[0]->amount_child,
				'adult' =>$data[0]->amount_adult,
				'start' =>$data[0]->date_start,
				'stop' =>$data[0]->date_end
			);
			return  view('backend/detail_book_boss',['information' => $information]);
		}
		return redirect('home');
	}
	public function de2boss($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='boss'){
			$data = DB::table('promotion')->where('id',$id)->get(); 
			return  view('backend/detail_promotion_boss',['data' => $data]);
		}
		return redirect('home');
	}
	public function de3boss($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='boss'){
			$data = DB::table('room')->where('id',$id)->get(); 
			return  view('backend/detail_room_boss',['data' => $data]);
		}
		return redirect('home');
	}
	//**************************************************
	public function employee(){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='employee'){
			$book = DB::table('booking')->get();
			$users = DB::table('booking')
			->join('users', 'booking.id_customer', '=', 'users.id')
			->select('name')
			->get();
			$room = DB::table('booking')
			->join('room', 'booking.id_room', '=', 'room.id')
			->select('type')
			->get();
			$ques = DB::table('question')->get();
			$name_ques = DB::table('question')
			->join('users', 'question.id_customer', '=', 'users.id')
			->select('name')
			->get();
			$data = array(
				'book' => $book,
				'name' => $users,
				'room' => $room,
				'ques' => $ques,
				'id' => $ques[0]->id,
				'name_ques' => $name_ques
			);
			return view('backend/employee' , ['data' => $data] );
		}
		return redirect('home');
	}
	public function employee1($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='employee'){
			$data = DB::table('booking')->where('id',$id)->get(); 
			$result = array(
				'status' => 'YES'
			);
			DB::table('booking')->where ('id',$id) ->update($result);
			return  redirect('employee');
		}
		return redirect('home');
	}
	public function employee2($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='employee'){
			$result = array(
				'status' => 'NO'
			);
			DB::table('booking')->where ('id',$id) ->update($result);
			return  redirect('employee');
		}
		return redirect('home');
	}
	public function employee3(Request $request){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='employee'){
			$data = $request->all();
			$id = $data['id'];
			$result = array(
				'answer'=>$data['text']
			);
			DB::table('question')->where ('id',$id) ->update($result);
			return  redirect('employee');
		}
		return redirect('home');
	}
	public function de1employee($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='employee'){
			$data = DB::table('booking')->where('id',$id)->get(); 
			$name = DB::table('users')->where('id',$data[0]->id_customer)->get();
			$room = DB::table('booking')
			->join('room', 'booking.id_room', '=', 'room.id')
			->select('type','price','name')
			->get();
			$infoemation = array(
				'start' => $data[0]->date_start,
				'stop' => $data[0]->date_end,
				'amount_child' => $data[0]->amount_child,
				'amount_adult' => $data[0]->amount_adult,
				'amount_room' => $data[0]->amount_room,
				'name_room' => $room[0]->name,
				'name' => $name[0]->name,
				'type' => $room[0]->type,

				'price' => $room[0]->price
			);
			return view('backend/detail_book_employee' , ['infoemation' => $infoemation] );
		}
		return redirect('home');
	}
	public function de2employee($id){
		session_start();
		if(isset($_SESSION['status'])&&$_SESSION['status']=='employee'){
			$ques = DB::table('question')->get();
			$name_ques = DB::table('question')
			->join('users', 'question.id_customer', '=', 'users.id')
			->select('name')
			->get();
			$data = array(
				'ques' => $ques[0]->question,
				'id' => $ques[0]->id,
				'name' => $name_ques[0]->name
			);
			return view('backend/detail_question_employee' , ['data' => $data] );
		}
		return redirect('home');
	}
}
