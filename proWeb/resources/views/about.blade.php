<?php
	session_start();
?>
<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css')?>

</head>
<body>
	<?=view('header')?>
	<div class="body">
		<?php
		if(isset($_SESSION['name'])){
			echo view('menulogin');
		}else{
			echo view('menu');
		}
		?>
		<div role="main" class="main">
			<section class="page-header page-header-custom-background parallax section section-parallax section-center m-0 section-overlay-opacity section-overlay-opacity-scale-3" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/hotel/parallax-hotel.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="font-weight-bold text-light text-uppercase">T & J Hotel <span>ยินดีต้อนรับ</span></h1>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item active">
										<a class="nav-link" href="#tabsNavigation1" data-toggle="tab"> ภาพรวม</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation2" data-toggle="tab"> บริการ</a>
									</li>
								</ul>
							</div>

							<a href="demo-hotel-special-offers.html" class="mt-4 mb-4 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="0" title="">
								<img alt="" class="img-fluid mt-2 mt-lg-4 mb-4" src="img/demos/hotel/banner-hotel.jpg">
							</a>
						</div>
						<div class="col-lg-9">
							<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">

								<h3 class="mb-0 pb-0 text-uppercase">T & J Hotel</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-8">

										<p class="lead">ท่ามกลางความวุ่นวายใจกลางเมืองนครราชสีมา เมืองอันเป็นประตูสู่ภูมิภาคอีสาน โรงแรมT&J ตั้งตระหง่านอยู่คู่ชาวจังหวัดนครราชสีมามาอย่างยาวนาน ด้วยบริการห้องพักที่มีจำนวนมากที่สุดในจังหวัด คอยต้อนรับแขกบ้านแขกเมืองคนสำคัญที่มีโอกาสแวะมาเยี่ยมเยือนจังหวัด</p>

										<p class="mt-4">นับตั้งแต่วินาทีแรกที่ย่างก้าวสู่โรงแรมT&J  จะได้สัมผัสถึงศิลปะวัฒนธรรมขอมประยุกต์ในมุมมองใหม่ ที่ถูกถักทอ เรียงร้อยออกสะท้อนเห็นถึงตำนานและประวัติศาตร์ที่มีมาอย่างยาวนาน สื่อสารผ่านสถาปัตยกรรมร่วมสมัยอย่างวิจิตรบรรจง รอคอยมอบประสบการณอันเป็นเอกลักษณ์์ชั้นยอด</p>

									</div>
									<div class="col-lg-4">

										<div class="owl-carousel owl-theme nav-inside box-shadow-custom mt-4" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}">
											<div>
												<img alt="" class="img-fluid" src="img/demos/hotel/gallery/gallery-1.jpg">
											</div>
											<div>
												<img alt="" class="img-fluid" src="img/demos/hotel/gallery/gallery-2.jpg">
											</div>
										</div>

									</div>
								</div>

								<div class="row mt-4">
									<div class="col-lg-4">
										<ul class="list list-icons list-primary list-borders text-uppercase text-color-dark text-2">
											<li><i class="fas fa-check"></i> 24 ห้อง, 4 ห้องสวีทสุดหรู</li>
											<li><i class="fas fa-check"></i> ศูนย์ออกกำลังกาย</li>
											<li><i class="fas fa-check"></i> บริการรับส่งจากสนามบิน</li>
										</ul>
									</div>
									<div class="col-lg-4">
										<ul class="list list-icons list-primary list-borders text-uppercase text-color-dark text-2">
											<li><i class="fas fa-check"></i> บริการอาหารในห้องพักตลอด 24 ชั่วโมง</li>
											<li><i class="fas fa-check"></i> บริการค็อกเทลบาร์</li>
											<li><i class="fas fa-check"></i> สัตว์เลี้ยงอยู่ฟรี</li>
										</ul>
									</div>
									<div class="col-lg-4">
										<ul class="list list-icons list-primary list-borders text-uppercase text-color-dark text-2">
											<li><i class="fas fa-check"></i> บริการนำรถไปจอด</li>
											<li><i class="fas fa-check"></i> บริการสระว่ายน้ำ</li>
											<li><i class="fas fa-check"></i> ฟรี Wi-Fi</li>
										</ul>
									</div>
								</div>

							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation2">

								<h3 class="mb-0 pb-0 text-uppercase">บริการและสิ่งอำนวยความสะดวก</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col">

										<p class="lead">ห้องพักทุกห้องประกอบด้วยสิ่งอำนวยความสะดวกที่คัดสรรมาแล้วอย่างดี ผู้เข้าพักจึงมั่นใจได้ว่าจะได้รับความสะดวกสบายสูงสุด ที่พักมีสิ่งอำนวยความสะดวกทางนันทนาการหลากหลายไว้คอยให้บริการ เช่น สระว่ายน้ำกลางแจ้ง, สระว่ายน้ำ (สำหรับเด็ก) เป็นต้น ไม่ว่าท่านจะเดินทางมาด้วยจุดประสงค์ใด โรงแรมT&J  คือที่พักที่ดีเยี่ยมที่สุดสำหรับการพักผ่อนอย่างสุขกายสบายใจ</p>

										<p class="mt-4">ผู้เข้าพักสามารถเพลิดเพลินกับบริการและสิ่งอำนวยความสะดวกเหนือระดับได้ ณ ที่พักเช่น บริการอาหารในห้องพักตลอด 24 ชั่วโมง , ฟรี Wi-Fi ทุกห้อง, แผนกต้อนรับ 24 ชั่วโมง, Wi-Fi ในพื้นที่สาธารณะ, ที่จอดรถ
										</p>

									</div>
								</div>

								<h4 class="mt-4 mb-4 pb-0 text-uppercase">บริการของโรงแรมT&J</h4>

								<div class="row">
									<div class="col-lg-6">

										<ul class="list list-icons list-primary">
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fas fa-check"></i> บริการอาหารในห้องพักตลอด 24 ชั่วโมง</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300"><i class="fas fa-check"></i> บริการค็อกเทลบาร์</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600"><i class="fas fa-check"></i> บริการนำรถไปจอด</li>
										</ul>

									</div>
									<div class="col-lg-6">

										<ul class="list list-icons list-primary">
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fas fa-check"></i>บริการรับส่งจากสนามบิน</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300"><i class="fas fa-check"></i> ศูนย์ออกกำลังกาย</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600"><i class="fas fa-check"></i> บริการสระว่ายน้ำ</li>
										</ul>

									</div>
								</div>

								<h4 class="mt-4 mb-4 pb-0 text-uppercase">สิ่งอำนวยความสะดวก</h4>

								<div class="row">
									<div class="col-lg-6">

										<ul class="list list-icons list-primary">
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300"><i class="fas fa-check"></i>ตู้นิรภัย</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600"><i class="fas fa-check"></i>บริการเบิกถอนเงินสด</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900"><i class="fas fa-check"></i>พื้นที่สูบบุหรี่</li>
										</ul>

									</div>
									<div class="col-lg-6">

										<ul class="list list-icons list-primary">
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0"><i class="fas fa-check"></i>ห้องซักรีด</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300"><i class="fas fa-check"></i>Wi-Fi ทุกห้อง (ฟรี)</li>
											<li class="appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600"><i class="fas fa-check"></i>Wi-Fi ในพื้นที่สาธารณะ</li>
										</ul>

									</div>
								</div>

							</div>

						</div>

					</div>
				</div>
			</section>

			

			<?=view('footer')?>

		</div>

	</div>

	<?=view('js')?>

</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$('#about').addClass('active');
	});
</script>