<?php session_start(); ?>
<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css')?>

</head>
<body>

	<?=view('header')?>
	<div class="body">
		<?php
		if(isset($_SESSION['name'])){
			echo view('menulogin');
		}else{
			echo view('menu');
		}
		?>

		<div role="main" class="main">

			<section class="page-header section section-primary section-no-border section-center page-header-custom-background m-0">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="font-weight-bold text-light text-uppercase">จองห้องพัก <span>จองห้องพักได้ที่นี่</span></h1>
						</div>
					</div>
				</div>
			</section>
			<?php
			if(!isset($_SESSION['name'])){
				?>
				<div class="container">
					<h1 class="font-weight-bold text-uppercase text-center mt-5 mb-5" style="color:#bc9552">กรุณาเข้าสู่ระบบ</h1>
				</div>
				<?php
			}else{
				?>
				<div class="container">
					<form action="/booking" method="post">
						{{ csrf_field() }}
						<div class="row mt-5 mb-5">
							<div class="col-lg-4">
								<section class="section section-tertiary section-no-border p-5 mt-1 mb-4" data-plugin-sticky data-plugin-options="{'minWidth': 991, 'containerSelector': '.container', 'padding': {'top': 150}}">
									<div class="form-row">
										<div class="form-group col">
											<h4 class="mt-4 mb-4 pb-0 text-uppercase">รายละเอียดการจอง</h4>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<div class="form-control-custom form-control-datepicker-custom">
												<input id="datepicker" class="form-control"name="startdate" value="<?=date("m/d/Y")?>"/>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<div class="form-control-custom form-control-datepicker-custom">
												<input id="datepicker2" class="form-control"name="enddate" value="<?=date("m/d/Y")?>"/>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<div class="form-control-custom">
												<select class="form-control text-uppercase text-2" name="bookNowAdults" data-msg-required="This field is required." id="bookNowAdults" required>
													<option value="">จำนวนผู้ใหญ่</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<div class="form-control-custom">
												<select class="form-control text-uppercase text-2" name="bookNowKids" data-msg-required="This field is required." id="bookNowKids" required>
													<option value="">จำนวนเด็ก</option>
													<option value="1">0</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<div class="form-control-custom">
												<input type="number" name="numroom" class="form-control text-uppercase text-2" placeholder="จำนวนห้อง">
											</div>
										</div>
									</div>
								</section>
							</div>
							<div class="col-lg-8">

								<section class="section section-quaternary section-no-border text-light p-5 mt-1 mb-4">
									<div class="row">
										<div class="col">
											<h4 class="mt-4 mb-4 pb-0 text-uppercase">เลือกห้องของคุณ</h4>
										</div>
									</div>
									<?php
									foreach ($rooms as $room) { 
										?>
										<div class="row">
											<div class="col-1 text-center">
												<label class="mt-4 mb-4">
													<input type="radio" checked="checked" name="bookNowRoom1" id="bookNowRoom1" value="<?=$room->id?>">
												</label>
											</div>
											<div class="col-2 d-none d-sm-block">
												<img src="<?=$room->image?>" class="img-fluid" alt="">
											</div>
											<div class="col-11 col-sm-9">
												<h5 class="mt-0 mb-0">Standard Room</h5>
												<div class="room-suite-info">
													<ul>
														<li><label>เตียง</label>	<span><?=$room->bed?></span></li>
														<li><label>ราคา</label> <strong><?=$room->price?> บาท</strong></li>
													</ul>
												</div>
											</div>
										</div>
										<?php 
									}
									?>
								</section>
								<div class="row">
									<div class="col">
										<input type="submit" value="จองเดี๋ยวนี้" class="btn btn-primary btn-lg btn-block text-uppercase p-4 mb-4">
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			<?php } ?>
			<?=view('footer')?>
		</div>

	</div>


	<?=view('js')?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$('#booknow').addClass('active');
	});
</script>
<script>
	$('#datepicker').datepicker({
		uiLibrary: 'bootstrap4'
	});
</script>
<script>
	$('#datepicker2').datepicker({
		uiLibrary: 'bootstrap4'
	});
</script>