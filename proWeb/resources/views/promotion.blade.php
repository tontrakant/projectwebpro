<?php session_start(); ?>
<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css')?>

</head>
<body>
	<?=view('header')?>
	<div class="body">
		<?php
		if(isset($_SESSION['name'])){
			echo view('menulogin');
		}else{
			echo view('menu');
		}
		?>
		<div role="main" class="main">
			<section class="page-header page-header-custom-background parallax section section-parallax section-center m-0 section-overlay-opacity section-overlay-opacity-scale-3" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/hotel/parallax-hotel-2.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="font-weight-bold text-light text-uppercase">โปรโมชั่นพิเศษ <span>โปรโมชั่นของโรงแรม T&J </span></h1>
						</div>
					</div>
				</div>
			</section>
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<?php
					foreach ($promotions as $promotion) {
						?>
						<div class="row">
							<div class="col">
								<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom box-shadow-custom">
									<span class="thumb-info-side-image-wrapper">
										<img alt="" class="img-fluid" style="max-width: 340px;" src="{{url($promotion->image)}}">
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">
											<h4 class="text-uppercase mb-1"><?=$promotion->name?></h4>
											<p><?=$promotion->description?></p>

										</span>
									</span>
								</span>
							</div>								
						</div>
						<?php 
					}
					?>
				</div>
			</section>
			<?=view('footer')?>
		</div>
	</div>
	<?=view('js')?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$('#promotion').addClass('active');
	});
</script>
