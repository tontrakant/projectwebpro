<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	


	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css')?>

</head>
<body>
	<?=view('header')?>
	<div class="body">
		<?=view('menu')?>
		<div role="main" class="main">

			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-4 offset-sm-7 mb-5 mt-5">
						<form action="/insert_user" method="post">
							{{ csrf_field() }} 
							<div class="form-group">
								<label for="InputUsername">ชื่อผู้ใช้</label>
								<input type="text" class="form-control" id="user" name="user">
							</div>
							<div class="form-group">
								<label for="InputPassword">รหัสผ่าน</label>
								<input type="password" class="form-control" id="pass" name="pass" >
							</div>
							<div class="form-group">
								<label for="InputName">ชื่อ-นามสกุล</label>
								<input type="text" class="form-control" id="name" name="name">
							</div>
							<div class="form-group">
								<label for="InputEmail1">เบอร์โทร</label>
								<input type="text" class="form-control" id="phone" name="phone">
							</div>
							<div class="form-group">
								<label for="InputEmail1">อีเมล์</label>
								<input type="email" class="form-control" id="email" name="email">
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
			</div>

			<?=view('footer')?>

		</div>

	</div>
	<?=view('js')?>


</body>
</html>

