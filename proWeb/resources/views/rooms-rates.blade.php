<?php session_start(); ?>
<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<?php
		if(isset($_SESSION['name'])){
			echo view('menulogin');
		}else{
			echo view('menu');
		}
		?>
		
		<div role="main" class="main">

			<section class="page-header section section-primary section-no-border section-center page-header-custom-background m-0">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="font-weight-bold text-light text-uppercase">Rooms &amp; Rates <span>Check out our amazing options</span></h1>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-no-background section-no-border m-0 p-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col">

							<div class="row">
								<div class="col d-flex justify-content-center">
									<ul class="nav nav-pills nav-pills-center sort-source text-2 text-uppercase my-4 py-2" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
										<li class="nav-item active" data-option-value="*"><a class="nav-link active" href="#">Show All</a></li>
										<li class="nav-item" data-option-value=".rooms"><a class="nav-link" href="#">Rooms</a></li>
										<li class="nav-item" data-option-value=".suites"><a class="nav-link" href="#">Suites</a></li>
									</ul>
								</div>
							</div>

							<div class="sort-destination-loader sort-destination-loader-showing">
								<div class="row mb-4 portfolio-list sort-destination" data-sort-id="portfolio">

									<?php
									foreach ($rooms as $room) {
										?>
										<div class="col-md-6 col-lg-4 isotope-item <?=$room->type?> mb-4 pb-3">
											<a href="demo-hotel-rooms-rates-detail.html">
												<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="<?=$room->image?>" class="img-fluid" alt="">
														<!-- <span class="thumb-info-title">
															<span class="thumb-info-inner">ดูรายละเอียด</span>
														</span> -->
													</span>
												</span>
											</a>
											<h5 class="mt-3 mb-0"><?=$room->name?></h5>
											<div class="room-suite-info">
												<ul>
													<li><label>เตียง</label>	<span><?=$room->bed?></span></li>
													<li><label>จำนวนผู้พัก</label> <span><?=$room->people?> คน</span></li>
													<li><label>ขนาด</label>	<span><?=$room->size?> ตารางเมตร</span></li>
													<li><label>ราคา</label> <strong><?=$room->price?> บาท</strong></li>
													<li>
														<!-- <a href="demo-hotel-rooms-rates-detail.html" class="room-suite-info-detail" title="">ดูรายละเอียด<i class="fas fa-long-arrow-alt-right"></i></a> -->
														<a href="/book" class="room-suite-info-book" title="">จองเดี๋ยวนี้</i></a>
													</li>
												</ul>
											</div>
										</div>
										<?php 
									}
									?>
								</div>
							</div>

						</div>

					</div>
				</div>
			</section>

			<?=view('footer');?>

		</div>

	</div>

	<?=view('js');?>

</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$('#roomrate').addClass('active');
	});
</script>