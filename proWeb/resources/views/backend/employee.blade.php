<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item">
										<a class="nav-link" href="/home"> หน้าแรก</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation1" data-toggle="tab"> รายการการจองห้อง</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation3" data-toggle="tab"> โปรโมชั่น</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation4" data-toggle="tab"> ห้องพัก</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation2" data-toggle="tab"> ตอบกลับลูกค้า</a>
									</li>
									<li class="nav-item">
										<a class="nav-link active" href="/logout" > ออกจากระบบ</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
								<h3 class="mb-0 pb-0 text-uppercase">รายการการจองห้อง</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ID การจองห้อง</th>
													<th>ชื่อ</th>
													<th>ประเภทห้อง</th>
													<th>จำนวนห้อง</th>
													<th>สถานะ</th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												if (count($data['book']) > 0) { 
													$i = 1 ;
													$j = 0 ;
													foreach ($data['book'] as $boo) {
														$name = $data['name'];
														$room = $data['room'];
														echo "<tr>"; 
														echo "<td>".$boo->id."</td>";
														echo "<td>".$name[$j]->name ."</td>";
														echo "<td>".$room[$j]->type."</td>";
														echo "<td>".$boo->amount_room ."</td>";  
														echo "<td>".$boo->status ."</td>";
														echo "<td><a href='de1employee/".$boo->id."' title=''>รายละเอียด</a></td>";
														echo "<td><a href='/employee1/".$boo->id."' title=''>ยืนยัน</a></td>";
														echo "<td><a href='/employee2/".$boo->id."' title=''>ยกเลิก</a></td>";
														echo "</tr>";
														$i++;
														$j++;

													}
												}
												else {
													echo "<tr><td align = 'center' colspan = '4'> Not Found Data</td></tr>";
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation2">
								<h3 class="mb-0 pb-0 text-uppercase">ตอบกลับลูกค้า</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ชื่อ</th>
													<th>คำถาม</th>
													<th>ตอบกลับ</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												if (count($data['ques']) > 0) {
													$j = 0 ;
													foreach ($data['ques'] as $ques) {
														$name = $data['name_ques'];
														echo "<tr>"; 
														echo "<td>".$name[$j]->name ."</td>";
														echo "<td>".$ques->question ."</td>";  
														echo "<td>".$ques->answer ."</td>";
														echo "<td><a href='de2employee/".$ques->id."' title=''>ตอบกลับ</a></td>";
														echo "</tr>";
														$j++;

													}
												}
												else {
													echo "<tr><td align = 'center' colspan = '4'> Not Found Data</td></tr>";
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
								<div class="row">
									<div class="col">
										<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom box-shadow-custom">
											<span class="thumb-info-side-image-wrapper">
												<img alt="" class="img-fluid" style="max-width: 340px;" src="img/demos/hotel/gallery/gallery-2.jpg">
											</span>
											<span class="thumb-info-caption">
												<span class="thumb-info-caption-text">
													<h4 class="text-uppercase mb-1">มาแต่ตัวทัวร์ยกแก๊ง</h4>
													<p>เมื่อท่านและแก๊งเพื่อนจองห้องพัก DELUXE ROOM จำนวน 2 ห้องขึ้นไป รับทันที อาหารเช้าฟรี โปรโมชั่น เริ่มตั้งแต่วันนี้ จนถึง 31 มกราคม 2562 </p>

												</span>
											</span>
										</span>
									</div>								
								</div>
								<div class="row">
									<div class="col">
										<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom box-shadow-custom">
											<span class="thumb-info-side-image-wrapper">
												<img alt="" class="img-fluid" style="max-width: 340px;" src="img/demos/hotel/gallery/gallery-1.jpg">
											</span>
											<span class="thumb-info-caption">
												<span class="thumb-info-caption-text">
													<h4 class="text-uppercase mb-1">คู่รักหวานแว่ว</h4>
													<p>หากท่านต้องการมาฮันนีมูล แล้วจองห้อง LUXURY SUITES เพียงท่านแสดงใบสมรส ลลดทันที 10 % โปรโมชั่น เริ่มตั้งแต่วันนี้ จนถึง 20 กุมภาพันธ์ 2562 </p>

												</span>
											</span>
										</span>
									</div>								
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation4">
								<div class="container">
									<div class="row mb-4">
										<div class="col">
											<div class="row">
												<div class="col d-flex justify-content-center">
													<ul class="nav nav-pills nav-pills-center sort-source text-2 text-uppercase my-4 py-2" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
														<li class="nav-item active" data-option-value="*"><a class="nav-link active" href="#">Show All</a></li>
														<li class="nav-item" data-option-value=".rooms"><a class="nav-link" href="#">Rooms</a></li>
														<li class="nav-item" data-option-value=".suites"><a class="nav-link" href="#">Suites</a></li>
													</ul>
												</div>
											</div>

											<div class="sort-destination-loader sort-destination-loader-showing">
												<div class="row mb-4 portfolio-list sort-destination" data-sort-id="portfolio">
													<div class="col-md-6 col-lg-4 isotope-item rooms mb-4 pb-3">
														<a href="demo-hotel-rooms-rates-detail.html">
															<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
																<span class="thumb-info-wrapper">
																	<img src="img/demos/hotel/room-1.jpg" class="img-fluid" alt="">
																	<span class="thumb-info-title">
																		<span class="thumb-info-inner">ดูรายละเอียด</span>
																	</span>
																</span>
															</span>
														</a>
														<h5 class="mt-3 mb-0">Standard Room</h5>
														<div class="room-suite-info">
															<ul>
																<li><label>เตียง</label>	<span>เตียงคู่ 1 เตียง</span></li>
																<li><label>จำนวนผู้พัก</label> <span>2 คน</span></li>
																<li><label>ขนาด</label>	<span>40 ตารางเมตร</span></li>
																<li><label>ราคา</label> <strong>2,000 บาท</strong></li>
																<li>
																	<a href="demo-hotel-rooms-rates-detail.html" class="room-suite-info-detail" title="">ดูรายละเอียด<i class="fas fa-long-arrow-alt-right"></i></a>
																	<a href="demo-hotel-book.html" class="room-suite-info-book" title="">จองเดี๋ยวนี้</i></a>
																</li>
															</ul>
														</div>
													</div>
													<div class="col-md-6 col-lg-4 isotope-item rooms mb-4 pb-3">
														<a href="demo-hotel-rooms-rates-detail.html">
															<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
																<span class="thumb-info-wrapper">
																	<img src="img/demos/hotel/room-2.jpg" class="img-fluid" alt="">
																	<span class="thumb-info-title">
																		<span class="thumb-info-inner">ดูรายละเอียด</span>
																	</span>
																</span>
															</span>
														</a>
														<h5 class="mt-3 mb-0">Premium Room</h5>
														<div class="room-suite-info">
															<ul>
																<li><label>เตียง</label>	<span>เตียงเดี่ยว 2 เตียง</span></li>
																<li><label>จำนวนผู้พัก</label> <span>2 คน</span></li>
																<li><label>ขนาด</label>	<span>50 ตารางเมตร</span></li>
																<li><label>ราคา</label> <strong>2,000 บาท</strong></li>
																<li>
																	<a href="demo-hotel-rooms-rates-detail.html" class="room-suite-info-detail" title="">ดูรายละเอียด<i class="fas fa-long-arrow-alt-right"></i></a>
																	<a href="demo-hotel-book.html" class="room-suite-info-book" title="">จองเดี๋ยวนี้</i></a>
																</li>
															</ul>
														</div>
													</div>
													<div class="col-md-6 col-lg-4 isotope-item rooms mb-4 pb-3">
														<a href="demo-hotel-rooms-rates-detail.html">
															<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
																<span class="thumb-info-wrapper">
																	<img src="img/demos/hotel/room-3.jpg" class="img-fluid" alt="">
																	<span class="thumb-info-title">
																		<span class="thumb-info-inner">ดูรายละเอียด</span>
																	</span>
																</span>
															</span>
														</a>
														<h5 class="mt-3 mb-0">Deluxe Room</h5>
														<div class="room-suite-info">
															<ul>
																<li><label>เตียง</label>	<span>เตียงเดี่ยว 2 เตียง</span></li>
																<li><label>จำนวนผู้พัก</label> <span>3 คน</span></li>
																<li><label>ขนาด</label>	<span>80 ตารางเมตร</span></li>
																<li><label>ราคา</label> <strong>2,500 บาท</strong></li>
																<li>
																	<a href="demo-hotel-rooms-rates-detail.html" class="room-suite-info-detail" title="">ดูรายละเอียด<i class="fas fa-long-arrow-alt-right"></i></a>
																	<a href="demo-hotel-book.html" class="room-suite-info-book" title="">จองเดี๋ยวนี้</i></a>
																</li>
															</ul>
														</div>
													</div>
													<div class="col-md-6 col-lg-4 isotope-item rooms mb-4 pb-3">
														<a href="demo-hotel-rooms-rates-detail.html">
															<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
																<span class="thumb-info-wrapper">
																	<img src="img/demos/hotel/room-4.jpg" class="img-fluid" alt="">
																	<span class="thumb-info-title">
																		<span class="thumb-info-inner">ดูรายละเอียด</span>
																	</span>
																</span>
															</span>
														</a>
														<h5 class="mt-3 mb-0">Executive Room</h5>
														<div class="room-suite-info">
															<ul>
																<li><label>เตียง</label>	<span>คิงไซส์</span></li>
																<li><label>จำนวนผู้พัก</label> <span>2 คน</span></li>
																<li><label>ขนาด</label>	<span>90 ตารางเมตร</span></li>
																<li><label>ราคา</label> <strong>3,000 บาท</strong></li>
																<li>
																	<a href="demo-hotel-rooms-rates-detail.html" class="room-suite-info-detail" title="">ดูรายละเอียด<i class="fas fa-long-arrow-alt-right"></i></a>
																	<a href="demo-hotel-book.html" class="room-suite-info-book" title="">จองเดี๋ยวนี้</i></a>
																</li>
															</ul>
														</div>
													</div>
													<div class="col-md-6 col-lg-4 isotope-item rooms mb-4 pb-3">
														<a href="demo-hotel-rooms-rates-detail.html">
															<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
																<span class="thumb-info-wrapper">
																	<img src="img/demos/hotel/room-5.jpg" class="img-fluid" alt="">
																	<span class="thumb-info-title">
																		<span class="thumb-info-inner">ดูรายละเอียด</span>
																	</span>
																</span>
															</span>
														</a>
														<h5 class="mt-3 mb-0">Superior Room</h5>
														<div class="room-suite-info">
															<ul>
																<li><label>เตียง</label>	<span>คิงไซส์ หรือ เตียงคู่ 2 เตียง</span></li>
																<li><label>จำนวนผู้พัก</label> <span>5 คน</span></li>
																<li><label>ขนาด</label>	<span>140 ตารางเมตร</span></li>
																<li><label>ราคา</label> <strong>3,000 บาท</strong></li>
																<li>
																	<a href="demo-hotel-rooms-rates-detail.html" class="room-suite-info-detail" title="">ดูรายละเอียด<i class="fas fa-long-arrow-alt-right"></i></a>
																	<a href="demo-hotel-book.html" class="room-suite-info-book" title="">จองเดี๋ยวนี้</i></a>
																</li>
															</ul>
														</div>
													</div>
													<div class="col-md-6 col-lg-4 isotope-item suites mb-4 pb-3">
														<a href="demo-hotel-rooms-rates-detail.html">
															<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
																<span class="thumb-info-wrapper">
																	<img src="img/demos/hotel/room-6.jpg" class="img-fluid" alt="">
																	<span class="thumb-info-title">
																		<span class="thumb-info-inner">ดูรายละเอียด</span>
																	</span>
																</span>
															</span>
														</a>
														<h5 class="mt-3 mb-0">Luxury Suites</h5>
														<div class="room-suite-info">
															<ul>
																<li><label>เตียง</label>	<span>คิงไซส์</span></li>
																<li><label>จำนวนผู้พัก</label> <span>7 คน</span></li>
																<li><label>ขนาด</label>	<span>150 ตารางเมตร</span></li>
																<li><label>ราคา</label> <strong>3,500 บาท</strong></li>
																<li>
																	<a href="demo-hotel-rooms-rates-detail.html" class="room-suite-info-detail" title="">ดูรายละเอียด<i class="fas fa-long-arrow-alt-right"></i></a>
																	<a href="demo-hotel-book.html" class="room-suite-info-book" title="">จองเดี๋ยวนี้</i></a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>

										</div>

									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>
			<?=view('footer');?>
		</div>

	</div>
	<?=view('js');?>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function() {
		$('#about').addClass('active');
	});
</script>
