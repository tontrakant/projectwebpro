<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs ">
									<li class="nav-item active">
										<a class="nav-link" href="/boss"> ย้อนกลับ</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
								<h3 class="mb-0 pb-0 text-uppercase">รายละเอียดห้องพัก</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>
								<div class="row">
									<div class="col-6 border">
										<div class="nav-link active">
											<img alt="" class="img-fluid mt-2  mr-10" src="{{asset('img/demos/hotel/admin-banner.png')}}">
											<br><br>
											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">ชื่อห้อง</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->name?></label>
												</div>
											</div>
											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">ประเภทเตียง</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->bed?></label>
												</div>
											</div>
											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">จำนวนผู้เข้าพัก</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->people?> คน</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-6 border">
										<div class="nav-link active">
											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">ราคาห้องพัก</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->price?> บาท</label>
												</div>
											</div>

											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">ขนาด</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->size?> ตารางเมตร</label>
												</div>
											</div>

											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">ประเภทห้องพัก</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->type?></label>
												</div>
											</div>

											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">ID โปรโมชั่น</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->id_promotion?></label>
												</div>
											</div>

											
											<div class="input-group">
												<div class="form-group">
													<label for="exampleFormControlSelect2" class="border text-success m-1 p-2">ID แอดมิน</label>
													<br>
													<label class="ml-3" for="exampleFormControlSelect2"><?=$data[0]->id_admin?></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>
			<?=view('footer');?>
		</div>

	</div>
	<?=view('js');?>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function() {
		$('#about').addClass('active');
	});
</script>
