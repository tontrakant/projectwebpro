<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section>
				<div class="container mt-5 mb-5">
					<a href="/admin" title="" class="btn text-light" style="background-color: #bc9552">กลับ</a>
				</div>
			</section>
			<section>
				<div class="container mt-5 mb-5">
					<h1>แก้ไขข้อมูลห้องพัก</h1>
					<form method="post" action="{{url('/updateroom')}}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-sm-6">
								@if (count($errors) > 0)
								<div class="alert alert-danger">
									Upload Validation Error<br><br>
									<ul>
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
								@endif
								<!-- @if ($message = Session::get('success')) -->
								<!-- <div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>
									<strong>{{ $message }}</strong>
								</div> -->
								<!-- <img src="/img/{{ Session::get('path') }}" width="300" /> -->
								<!-- @endif -->

								<div class="form-group">
									<table class="table">
										<tr>
											<td width="40%" align="right"><label>Select Image</label></td>
											<td width="30"><input id="fileToUpload" type="file" name="select_file"/></td>
										</tr>
									</table>
								</div>

								<div class="form-group">
									<img id="showimg" src="{{url($rooms[0]->image)}}" alt="" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="hidden" class="form-control" id="id" name="id" value="<?=$rooms[0]->id?>">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">ชื่อห้อง</label>
									<input type="test" class="form-control" id="nameroom" name="nameroom" value="<?=$rooms[0]->name?>">
								</div>
								<div class="form-group">
									<label for="inputState">ประเภทห้อง</label>
									<select id="inputState" class="form-control" name="type">
										<?php
										if($rooms[0]->type=='rooms'){
											?>
											<option>เลือก</option>
											<option selected>rooms</option>
											<option>suites</option>
											<?php
										}
										else if($rooms[0]->type=='suites'){
											?>
											<option>เลือก</option>
											<option>rooms</option>
											<option selected>suites</option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label>เตียง</label>
									<select id="inputState" class="form-control" name="bed">
										<?php
										if($rooms[0]->bed=='เตียงคู่ 1 เตียง'){
											?>
											<option >เลือก</option>
											<option selected>เตียงคู่ 1 เตียง</option>
											<option >เตียงเดี่ยว 2 เตียง</option>
											<option >คิงไซส์</option>
											<option >คิงไซส์ หรือ เตียงคู่ 2 เตียง</option>
											<?php
										}
										else if($rooms[0]->bed=='เตียงเดี่ยว 2 เตียง'){
											?>
											<option >เลือก</option>
											<option >เตียงคู่ 1 เตียง</option>
											<option selected>เตียงเดี่ยว 2 เตียง</option>
											<option >คิงไซส์</option>
											<option >คิงไซส์ หรือ เตียงคู่ 2 เตียง</option>
											<?php
										}else if($rooms[0]->bed=='คิงไซส์'){
											?>
											<option >เลือก</option>
											<option >เตียงคู่ 1 เตียง</option>
											<option >เตียงเดี่ยว 2 เตียง</option>
											<option selected>คิงไซส์</option>
											<option >คิงไซส์ หรือ เตียงคู่ 2 เตียง</option>
											<?php
										}else if($rooms[0]->bed=='คิงไซส์ หรือ เตียงคู่ 2 เตียง'){
											?>
											<option >เลือก</option>
											<option >เตียงคู่ 1 เตียง</option>
											<option >เตียงเดี่ยว 2 เตียง</option>
											<option >คิงไซส์</option>
											<option selected>คิงไซส์ หรือ เตียงคู่ 2 เตียง</option>
											<?php
										}?>
									</select>
								</div>
								<div class="form-group">
									<label>จำนวนผู้เข้าพัก</label>
									<select id="inputState" class="form-control" name="numpeo">
										<?php
										if($rooms[0]->people==1){
											?>
											<option >เลือก</option>
											<option selected>1</option>
											<option >2</option>
											<option >3</option>
											<option >4</option>
											<option >5</option>
											<option >6</option>
											<option >7</option>
											<?php
										}else if($rooms[0]->people==2){
											?>
											<option >เลือก</option>
											<option >1</option>
											<option selected>2</option>
											<option >3</option>
											<option >4</option>
											<option >5</option>
											<option >6</option>
											<option >7</option>
											<?php
										}else if($rooms[0]->people==3){
											?>
											<option >เลือก</option>
											<option >1</option>
											<option >2</option>
											<option selected>3</option>
											<option >4</option>
											<option >5</option>
											<option >6</option>
											<option >7</option>
											<?php
										}else if($rooms[0]->people==4){
											?>
											<option >เลือก</option>
											<option >1</option>
											<option >2</option>
											<option >3</option>
											<option selected>4</option>
											<option >5</option>
											<option >6</option>
											<option >7</option>
											<?php
										}else if($rooms[0]->people==5){
											?>
											<option >เลือก</option>
											<option >1</option>
											<option >2</option>
											<option >3</option>
											<option >4</option>
											<option selected>5</option>
											<option >6</option>
											<option >7</option>
											<?php
										}else if($rooms[0]->people==6){
											?>
											<option >เลือก</option>
											<option >1</option>
											<option >2</option>
											<option >3</option>
											<option >4</option>
											<option >5</option>
											<option selected>6</option>
											<option >7</option>
											<?php
										}else if($rooms[0]->people==7){
											?>
											<option >เลือก</option>
											<option >1</option>
											<option >2</option>
											<option >3</option>
											<option >4</option>
											<option >5</option>
											<option >6</option>
											<option selected>7</option>
											<?php
										}else{
											?>
											<option selected>เลือก</option>
											<option >1</option>
											<option >2</option>
											<option >3</option>
											<option >4</option>
											<option >5</option>
											<option >6</option>
											<option >7</option>
											<?php
										}?>
									</select>
								</div>
								<div class="form-group">
									<label>ขนาดห้อง</label>
									<div class="form-row">
										<div class="col">
											<input type="number" class="form-control" id="nameroom" name="size" value="<?=$rooms[0]->size?>">
										</div>
										<div class="col">
											<label for="">ตารางเมตร</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="">ราคา</label>
									<div class="form-row">
										<div class="col">
											<input type="number" class="form-control" id="nameroom" name="price" value="<?=$rooms[0]->price?>">
										</div>
										<div class="col">
											<label for="">บาท</label>
										</div>
									</div>
								</div>
								<td width="30%" align="left"><input type="submit" name="upload" class="btn btn-primary" value="บันทึก"></td>
							</div>

						</div>
					</form>
				</div>
			</section>
		</dir>
		<?=view('footer');?>
	</div>

</div>
<?=view('js');?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#showimg').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
		$('#fileToUpload').change(function(event) {
			/* Act on the event */
			readURL(this);
		});
	});
</script>