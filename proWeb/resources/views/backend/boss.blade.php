<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item">
										<a class="nav-link" href="/home" > หน้าแรก</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation1" data-toggle="tab"> การจองห้อง</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation3" data-toggle="tab"> โปรโมชั่น</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation4" data-toggle="tab"> รายละเอียดห้อง</a>
									</li>
									<li class="nav-item">
										<a class="nav-link active" href="/logout" > ออกจากระบบ</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
								<h3 class="mb-0 pb-0 text-uppercase">รายการการจองห้อง</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ID การจองห้อง</th>
													<th>ชื่อผู้จอง</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												if (count($data['book']) > 0) { 
													$i = 1 ;
													$j = 0 ;
													foreach ($data['book'] as $boo) {
														$d = $data['name'];
														echo "<tr>"; 
														echo "<td>".$boo->id."</td>";
														echo "<td>".$d[$j]->name ."</td>"; 
														echo "<td><a href='de1boss/".$boo->id."' title=''>รายละเอียด</a></td>";
														echo "</tr>";
														$i++;
														$j++;

													}
												}
												else {
													echo "<tr><td align = 'center' colspan = '4'> Not Found Data</td></tr>";
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
								<h3 class="mb-0 pb-0 text-uppercase">โปรโมชั่น</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ID</th>
													<th>ชื่อโปรโมชั่น</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												if (count($data['promotion']) > 0) { 
													$i = 1 ;
													foreach ($data['promotion'] as $promo) {
															echo "<tr>"; // tr แถวนึงของtable
															echo "<td>".$promo->id."</td>"; // td คอลัมนึง
															echo "<td>".$promo->name."</td>";
															echo "<td><a href='de2boss/".$promo->id."' title=''>รายละเอียด</a></td>";
															echo "</tr>";
															$i++;
														}
													}
													else {
														echo "<tr><td align = 'center' colspan = '4'> Not Found Data</td></tr>";
													}
													?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="tab-pane tab-pane-navigation" id="tabsNavigation4">
									<h3 class="mb-0 pb-0 text-uppercase">รายละเอียดห้องพัก</h3>
									<div class="divider divider-primary divider-small mb-4 mt-0">
										<hr class="mt-2 mr-auto">
									</div>

									<div class="row">
										<div class="col-lg-12">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>ID ห้อง</th>
														<th>ประเภทห้อง</th>
														<th>ชื่อห้อง</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?php 
													if (count($data['room']) > 0) { 
														$i = 1 ;
														foreach ($data['room'] as $roo) {
															echo "<tr>"; 
															echo "<td>".$roo->id."</td>"; 
															echo "<td>".$roo->type."</td>";
															echo "<td>".$roo->name."</td>";
															echo "<td><a href='de3boss/".$roo->id."' title=''>รายละเอียด</a></td>";
															echo "</tr>";
															$i++;
														}
													}
													else {
														echo "<tr><td align = 'center' colspan = '4'> Not Found Data</td></tr>";
													}
													?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</section>
				<?=view('footer');?>
			</div>

		</div>
		<?=view('js');?>
	</body>
	</html>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#about').addClass('active');
		});
	</script>
