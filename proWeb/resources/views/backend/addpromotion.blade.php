<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section>
				<div class="container mt-5 mb-5">
					<a href="/admin" title="" class="btn text-light" style="background-color: #bc9552">กลับ</a>
				</div>
			</section>
			<section>
				<div class="container mt-5 mb-5">
					<h1>เพิ่มโปรโมชัน</h1>
					<form method="post" action="{{url('/adddatapromotion')}}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-sm-6">
								@if (count($errors) > 0)
								<div class="alert alert-danger">
									Upload Validation Error<br><br>
									<ul>
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
								@endif
								<!-- @if ($message = Session::get('success')) -->
								<div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>
									<strong>{{ $message }}</strong>
								</div>
								<!-- <img src="/img/{{ Session::get('path') }}" width="300" /> -->
								<!-- @endif -->

								<div class="form-group">
									<table class="table">
										<tr>
											<td width="40%" align="right"><label>Select Image</label></td>
											<td width="30"><input id="fileToUpload" type="file" name="select_file" /></td>
										</tr>
									</table>
								</div>

								<div class="form-group">
									<img id="showimg" src="" alt="" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-6">

								<div class="form-group">
									<label for="exampleInputEmail1">ชื่อห้อง</label>
									<input type="test" class="form-control" id="name" name="name">
								</div>
								<div class="form-group">
									<label for="">คำอธิบาย</label>
									<textarea class="form-control" name="discription" rows="3"></textarea>
								</div>
								<div class="form-group">
									<label>วันที่สิ้นสุดโปรโมชั่น</label>
									<input id="datepicker" class="form-control" id="enddate" name="enddate" />
								</div>
								<td width="30%" align="left"><input type="submit" name="upload" class="btn btn-primary" value="ตกลง"></td>
							</div>

						</div>
					</form>
				</div>
			</section>
		</dir>
		<?=view('footer');?>
	</div>

</div>
<?=view('js');?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#showimg').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
		$('#fileToUpload').change(function(event) {
			/* Act on the event */
			readURL(this);
		});
	});
</script>
<script>
	$('#datepicker').datepicker({
		uiLibrary: 'bootstrap4'
	});
</script>