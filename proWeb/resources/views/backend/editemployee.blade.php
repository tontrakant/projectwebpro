<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section>
				<div class="container mt-5 mb-5">
					<a href="/admin" title="" class="btn text-light" style="background-color: #bc9552">กลับ</a>
				</div>
			</section>
			<section>
				<div class="container mt-5 mb-5">
					<h1>เพิ่มข้อมูลพนักงาน</h1>
					<form action="/updateemployee" method="post">
						{{ csrf_field() }}
						<div class="row">
							
							<div class="col-sm-6 offset-sm-6">
								<div class="form-group">
									<input type="hidden" class="form-control" id="id" name="id" value="<?=$employees[0]->id?>">
								</div>
								<div class="form-group">
									<label for="">ชื่อ</label>
									<input type="text" class="form-control" name="name" value="<?=$employees[0]->name?>">
								</div>
								<div class="form-group">
									<label for="">ชื่อผู้ใช้</label>
									<input type="text" class="form-control" name="username" value="<?=$employees[0]->username?>">
								</div>
								<div class="form-group">
									<label for="">รหัสผ่าน</label>
									<input type="text" class="form-control" name="password" value="<?=$employees[0]->password?>">
								</div>
								<div class="form-group">
									<label for="">ที่อยู่</label>
									<textarea class="form-control" rows="3" name="address" ><?=$employees[0]->address?></textarea>
								</div>
								<div class="form-group">
									<label for="">เบอร์โทร</label>
									<input type="text" class="form-control" name="phone" value="<?=$employees[0]->phone?>">
								</div>
								<div class="form-group">
									<label for="">อีเมล์</label>
									<input type="text" class="form-control" name="email" value="<?=$employees[0]->email?>">
								</div>
								<button type="submit" class="btn btn-primary">ตกลง</button>
							</div>
						</div>
					</form>
				</div>
			</section>
		</dir>
		<?=view('footer');?>
	</div>

</div>
<?=view('js');?>
</body>
</html>