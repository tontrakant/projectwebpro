<div class="header-top-custom">
	<div class="container mb-4 ">
		<div class="row">
			<div class="col-sm-5 offset-sm-4 col-address">
				<i class="icon-location-pin icons text-color-primary"></i>
				<label>ที่อยู่</label>
				<strong>ตำบล ในเมือง อำเภอเมืองนครราชสีมา นครราชสีมา</strong>
			</div>
			<div class="col-sm-3 col-phone">
				<i class="icon-phone icons text-color-primary"></i>
				<label>โทร</label>
				<strong>(800) 1234-5678</strong>
			</div>
		</div>
	</div>
</div>