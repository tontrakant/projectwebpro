<?php
session_start();
?>
<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css')?>

</head>
<body>

	<?=view('header')?>

	<div class="body">
		<?php
		if(isset($_SESSION['name'])){
			echo view('menulogin');
		}else{
			echo view('menu');
		}
		?>

		<div role="main" class="main">
			<div class="slider-container rev_slider_wrapper" style="height: 530px;">
				<div id="revolutionSlider" class="slider rev_slider manual" data-version="5.4.7">
					<ul>
						<li data-transition="boxfade">

							<img src="img/demos/hotel/slides/slide-hotel-1.jpg"  
							alt=""
							data-bgposition="center bottom"
							data-bgfit="cover"
							data-bgrepeat="no-repeat"
							data-bgparallax="10"
							class="rev-slidebg"
							data-no-retina>
						</li>
						<li data-transition="boxfade">

							<img src="img/demos/hotel/slides/slide-hotel-2.jpg"  
							alt=""
							data-bgposition="center bottom"
							data-bgfit="cover"
							data-bgrepeat="no-repeat"
							data-bgparallax="10"
							class="rev-slidebg"
							data-no-retina>
						</li>
						<li data-transition="boxfade">

							<img src="img/demos/hotel/slides/slide-hotel-3.jpg"  
							alt=""
							data-bgposition="center bottom"
							data-bgfit="cover"
							data-bgrepeat="no-repeat"
							data-bgparallax="10"
							class="rev-slidebg"
							data-no-retina>
						</li>
					</ul>
				</div>
			</div>
			<section class="parallax section section-parallax section-center mt-5 section-overlay-opacity section-overlay-opacity-scale-4" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/hotel/parallax-hotel.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h3 class="mb-1 mt-0 text-light text-uppercase">Enjoy the best of Porto</h3>
							<p class="lead text-light mb-4">Make your reservation right now with the best price</p>

							<a class="btn btn-primary btn-lg text-2 text-uppercase mt-2" href="/book">Book Now</a>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row">
						<div class="col-lg-4">

							<div class="owl-carousel owl-theme nav-inside box-shadow-custom mt-4" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}">
								<div>
									<img alt="" class="img-fluid" src="img/demos/hotel/gallery/gallery-1.jpg">
								</div>
								<div>
									<img alt="" class="img-fluid" src="img/demos/hotel/gallery/gallery-2.jpg">
								</div>
							</div>

						</div>
						<div class="col-lg-8">

							<h3 class="mt-4 mb-0 pb-0 text-uppercase">Hotel Overview</h3>
							<div class="divider divider-primary divider-small mb-4 mt-0">
								<hr class="mt-2 mr-auto">
							</div>

							<p class="mt-4">ท่ามกลางความวุ่นวายใจกลางเมืองนครราชสีมา เมืองอันเป็นประตูสู่ภูมิภาคอีสาน โรงแรมT&J ตั้งตระหง่านอยู่คู่ชาวจังหวัดนครราชสีมามาอย่างยาวนาน ด้วยบริการห้องพักที่มีจำนวนมากที่สุดในจังหวัด คอยต้อนรับแขกบ้านแขกเมืองคนสำคัญที่มีโอกาสแวะมาเยี่ยมเยือนจังหวัด <a href="/about" class="custom-rtl-link-fix" title="">(ดูทั้งหมด...)</a></p>

							<div class="row">
								<div class="col-lg-4">
									<ul class="list list-icons list-primary list-borders text-uppercase text-color-dark text-2">
										<li><i class="fas fa-check"></i> 24 ห้อง, 4 ห้องสวีทสุดหรู</li>
										<li><i class="fas fa-check"></i> ศูรย์ออกกำลังกาย</li>
										<li><i class="fas fa-check"></i> บริการรับส่งจากสนามบิน</li>
									</ul>
								</div>
								<div class="col-lg-4">
									<ul class="list list-icons list-primary list-borders text-uppercase text-color-dark text-2">
										<li><i class="fas fa-check"></i> บริการอาหารในห้องพักตลอด 24 ชั่วโมง </li>
										<li><i class="fas fa-check"></i> บริการค็อกเทลบาร์ </li>
										<li><i class="fas fa-check"></i> สัตว์เลี้ยงอยู่ฟรี </li>
									</ul>
								</div>
								<div class="col-lg-4">
									<ul class="list list-icons list-primary list-borders text-uppercase text-color-dark text-2">
										<li><i class="fas fa-check"></i> บริการนำรถไปจอด </li>
										<li><i class="fas fa-check"></i> บริการสระว่านน้ำ </li>
										<li><i class="fas fa-check"></i> ฟรี Wi-Fi</li>
									</ul>
								</div>
							</div>

						</div>


					</div>
				</div>
			</section>
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">

							<h3 class="mb-0 pb-0 text-uppercase">โปรโมชั่น</h3>
							<div class="divider divider-primary divider-small divider-small-center mb-4 mt-0">
								<hr class="mt-2">
							</div>
							<?php
							$promotion = DB::table('promotion')->get(); 
							?>
							<div class="row pt-4">
								<div class="col-lg-6">
									<?php if(count($promotion)>=1){
										?>
										<a href="#" class="text-decoration-none">
											<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom box-shadow-custom thumb-info-no-zoom thumb-info-side-image-custom-highlight appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="0">
												<span class="thumb-info-side-image-wrapper">
													<img alt="" class="img-fluid" src="{{url($promotion[0]->image)}}">
												</span>
												<span class="thumb-info-caption">
													<span class="thumb-info-caption-text">
														<h4 class="text-uppercase"><?=$promotion[0]->name?></h4>
														<p><?=$promotion[0]->description?></p>
													</span>
												</span>
											</span>
										</a>
										<?php
									} 
									?>
								</div>
								<div class="col-lg-6">
									<?php if(count($promotion)>=2){ ?>
										<a href="#" class="text-decoration-none">
											<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom box-shadow-custom appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="300">
												<span class="thumb-info-side-image-wrapper">
													<img alt="" class="img-fluid" style="max-width: 225px;" src="{{url($promotion[1]->image)}}">
												</span>
												<span class="thumb-info-caption">
													<span class="thumb-info-caption-text">
														<h4 class="text-uppercase"><?=$promotion[1]->name?></h4>
														<p><?=$promotion[1]->description?></p>
													</span>
												</span>
											</span>
										</a>
									<?php } 
									if(count($promotion)>=3){
										?>
										<a href="#" class="text-decoration-none">
											<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom box-shadow-custom appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
												<span class="thumb-info-side-image-wrapper">
													<img alt="" class="img-fluid" style="max-width: 225px;" src="{{url($promotion[2]->image)}}">
												</span>
												<span class="thumb-info-caption">
													<span class="thumb-info-caption-text">
														<h4 class="text-uppercase"><?=$promotion[2]->name?></h4>
														<p><?=$promotion[2]->description?></p>
													</span>
												</span>
											</span>
										</a>
									<?php } ?>
								</div>
							</div>

							<a class="btn btn-primary btn-lg text-2 text-uppercase mt-2" href="/promotion">ดูทั้งหมด</a>

						</div>

					</div>
				</div>
			</section>
			<?=view('footer');?>
		</div>
	</div>
	<?=view('js')?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$('#home').addClass('active');
	});
</script>
