<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item ">
										<a class="nav-link" href="/home" >หน้าแรก</a>
									</li>
									<li class="nav-item ">
										<a class="nav-link" href="#tabsNavigation1" data-toggle="tab"> รายการการจองห้อง</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
								<h3 class="mb-0 pb-0 text-uppercase">รายการการจองห้อง</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ชื่อห้อง</th>
													<th>จำนวนห้อง</th>
													<th>วันที่เข้าพัก</th>
													<th>วันที่ออก</th>
													<th>สถานะ</th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												if (count($data['book']) > 0) { 
													$j = 0 ;
													foreach ($data['book'] as $boo) {
														$room = $data['room'];
														if(count($room) == 0)break;
														echo "<tr>"; 
														echo "<td>".$room[$j]->name."</td>";
														echo "<td>".$boo->amount_room ."</td>";  
														echo "<td>".$boo->date_start ."</td>"; 
														echo "<td>".$boo->date_end ."</td>"; 
														echo "<td>".$boo->status ."</td>";
														echo "<td><a href='detailstatus/".$boo->id."' title=''>รายละเอียด</a></td>";
														$j++;

													}
												}
												else {
													echo "<tr><td align = 'center' colspan = '4'> Not Found Data</td></tr>";
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							
						</div>

					</div>
				</div>
			</section>
			<?=view('footer');?>
		</div>

	</div>
	<?=view('js');?>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function() {
		$('#about').addClass('active');
	});
</script>
