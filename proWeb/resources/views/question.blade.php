<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item">
										<a class="nav-link" href="/home"> หน้าแรก</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href=""> คำถาม</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9">
							<h3 class="mb-0 pb-0 text-uppercase">รายการคำถามทั้งหมด</h3>
							<div class="divider divider-primary divider-small mb-4 mt-0">
								<hr class="mt-2 mr-auto">
							</div>
							<div class="row">
								<div class="col-lg-12">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>คำถาม</th>
												<th>ตอบกลับ</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if (count($data['ques']) > 0) {
												$j = 0 ;
												foreach ($data['ques'] as $ques) {
													echo "<tr>"; 
													echo "<td>".$ques->question ."</td>";  
													echo "<td>".$ques->answer ."</td>";
													$j++;

												}
											}
											else {
												echo "<tr><td align = 'center' colspan = '4'> Not Found Data</td></tr>";
											}
											?>
										</tbody>
									</table>
									<a href="/addquestion" title="">เพิ่มคำถาม</a>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>
			<?=view('footer');?>
		</div>

	</div>
	<?=view('js');?>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function() {
		$('#about').addClass('active');
	});
</script>
