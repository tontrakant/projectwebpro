<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 220, 'stickyChangeLogo': false}">
	<div class="header-body">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo">
							<a href="/">
								<img alt="Porto" width="192" height="55" src="img/demos/hotel/logo-hotel.png">
							</a>
						</div>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<div class="header-nav header-nav-stripe">
							<div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
								<nav class="collapse">
									<ul class="nav nav-pills" id="mainNav">
										<li>
											<a id="home" class="nav-link" href="/home">
												Home
											</a>
										</li>
										<li>
											<a id="about" class="nav-link" href="/about">
												About
											</a>
										</li>
										<li>
											<a id="roomrate" class="nav-link" href="/roomsrates">
												Rooms &amp; Rates
											</a>
										</li>
										<li>
											<a id="promotion" class="nav-link" href="/promotion">
												Promotion
											</a>
										</li>
										<li>
											<a id="location" class="nav-link" href="location">
												Location
											</a>
										</li>
										<li >
											<a id="booknow" class="nav-link" href="/book">
												Book Now
											</a>
										</li>
										<li class="dropdown dropdown-full-color dropdown-primary dropdown-mega dropdown-mega-book-now" id="headerBookNow">
											<a id="login" class="nav-link dropdown-toggle" href="#">
												<?=$_SESSION['name']?>
											</a>
											<ul class="dropdown-menu">
												<li>
													<div class="dropdown-mega-content">
														<?php 
														if($_SESSION['status']!='user'){
															?>
															<form action="/<?=$_SESSION['status']?>">
																<div class="form-row">
																	<div class="form-group col mt-0">
																		<input type="submit" value="Backend" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
																	</div>
																</div>
															</form>
															<?php
														}else{
															?>
															<form action="/status">
																<div class="form-row">
																	<div class="form-group col mb-0">
																		<input type="submit" value="Status" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
																	</div>
																</div>
															</form>
															<form action="/question">
																<div class="form-row">
																	<div class="form-group col mt-3">
																		<input type="submit" value="Question" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
																	</div>
																</div>
															</form>
														<?php } ?>
														<form action='/logout' method='get'>
															{{ csrf_field() }} 
															<div class="form-row ">
																<div class="form-group col mt-0">
																	<input type="submit" value="Logout" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
																</div>
															</div>
														</form>
													</div>

												</li>

											</ul>

										</li>
									</ul>
								</nav>
							</div>
							<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
								<i class="fas fa-bars"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>