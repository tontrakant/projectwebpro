<?php
session_start();
?>
<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css')?>

</head>
<body>

	<?=view('header')?>

	<div class="body">
		<?php
		if(isset($_SESSION['name'])){
			echo view('menulogin');
		}else{
			echo view('menu');
		}
		?>

		<div role="main" class="main">
			<div class="slider-container rev_slider_wrapper" style="height: 530px;">
				<div id="revolutionSlider" class="slider rev_slider manual" data-version="5.4.7">
					<ul>
						<li data-transition="boxfade">

							<img src="img/demos/hotel/slides/slide-hotel-1.jpg"  
							alt=""
							data-bgposition="center bottom"
							data-bgfit="cover"
							data-bgrepeat="no-repeat"
							data-bgparallax="10"
							class="rev-slidebg"
							data-no-retina>
						</li>
						<li data-transition="boxfade">

							<img src="img/demos/hotel/slides/slide-hotel-2.jpg"  
							alt=""
							data-bgposition="center bottom"
							data-bgfit="cover"
							data-bgrepeat="no-repeat"
							data-bgparallax="10"
							class="rev-slidebg"
							data-no-retina>
						</li>
						<li data-transition="boxfade">

							<img src="img/demos/hotel/slides/slide-hotel-3.jpg"  
							alt=""
							data-bgposition="center bottom"
							data-bgfit="cover"
							data-bgrepeat="no-repeat"
							data-bgparallax="10"
							class="rev-slidebg"
							data-no-retina>
						</li>
					</ul>
				</div>
			</div>
			<div class="container-fluid" >
				<div class="m-4" id="map" style="height: 600px">
					
				</div>
			</div>
			<?=view('footer');?>
		</div>
	</div>
	<?=view('js')?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$('#location').addClass('active');
	});
</script>
<script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: 14.979579, lng: 102.097750};
  // The map, centered at Uluru
  var map = new google.maps.Map(
  	document.getElementById('map'), {zoom: 16, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
</script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
-->
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy894gggvUSyqrW01lYL7Ro8d-mdFzhC4&callback=initMap">
</script>